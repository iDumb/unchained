//
//  CertificatesTableViewController.swift
//  Unchained
//
//  Created by Ammad on 20/05/2020.
//  Copyright © 2020 Unchained. All rights reserved.
//

import UIKit
import Network

class CertificatesTableViewController: UITableViewController {
    
    var certifications: [Certification] = []
    var numberOfCertifications = 0
    var password: String?
    
    var isConnected: Bool?
    let monitor = NWPathMonitor()
    
    let userDefaults = UserDefaults.standard

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //check if user is connected to the network
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                print("Connected to a network")
                self.isConnected = true
            } else if path.status == .unsatisfied {
                print("Not connected to a network")
                self.isConnected = false
            }
        }
        
        let queue = DispatchQueue(label: "Monitor")
        monitor.start(queue: queue)
        
        //Load persistency
        numberOfCertifications = userDefaults.integer(forKey: "numberOfCertifications")
        
        let certificationsData = UserDefaults.standard.data(forKey: "certifications")
        
        if certificationsData != nil {
        certifications = try! JSONDecoder().decode([Certification].self, from: certificationsData!)
        }

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    @IBAction func addCertification(_ sender: UIBarButtonItem) {
        
        //if user is disconnected returns an alert
        if isConnected == false {
            let alertControllerConnection = UIAlertController(title: "No connection", message: "Please connect to a network", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default)
            alertControllerConnection.addAction(okAction)
            self.present(alertControllerConnection, animated: true, completion: nil)
            
            return
        }
        
        //present an alert to the user for password input
        let alertController = UIAlertController(title: "Insert Password", message: "Type the password you received from your school or teacher", preferredStyle: .alert)
        alertController.addTextField(configurationHandler: {
            (_ textField: UITextField) -> Void in
            textField.textAlignment = .center
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let saveAction = UIAlertAction(title: "Confirm", style: .default) {
            _ in
            self.password = alertController.textFields?.first?.text ?? ""
            
            self.pullCertification()
            
        }
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        self.present(alertController, animated: true, completion: nil)
                
        
        
    }
    
    //Pulls certification from blockchain using specified password
    func pullCertification() {
        
        let certification = Certification()
        let smartContractInteractions = SmartContractInteraction()
        
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        activityIndicator.center = self.view.center
        activityIndicator.style = .large
        
        self.view.addSubview(activityIndicator)
        
        smartContractInteractions.getDataFromSmartContract(password: password!)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            
            certification.name = smartContractInteractions.getCertificationName()
            certification.studentName = smartContractInteractions.getCertificationStudentName()
            certification.schoolAddress = smartContractInteractions.getCertificationSchoolAddress()
            certification.grade = smartContractInteractions.getCertificationGrade()
            certification.studentAddress = smartContractInteractions.getCertificationStudentAddress()
            certification.imageLink = smartContractInteractions.getCertificationImageLink()
            certification.schoolName = smartContractInteractions.getCertificationSchoolName()
            
            //Checks if a certification was correctly pulled
            if certification.name != "" && certification.studentAddress != nil {
            self.certifications.append(certification)
            self.numberOfCertifications += 1
                
            //persistency
            let certificationsData = try! JSONEncoder().encode(self.certifications)
            self.userDefaults.set(certificationsData, forKey: "certifications")
            self.userDefaults.set(self.numberOfCertifications, forKey: "numberOfCertifications")
            }
            
            else {
                let alertController = UIAlertController(title: "Error", message: "Inserted password is not associated with any certification, please try again", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                
            }
            
            self.tableView.reloadData()
            activityIndicator.stopAnimating()
            
        }
        
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfCertifications
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let certification = certifications[indexPath.row]
        // Configure the cell...
        
        cell.imageView?.image = #imageLiteral(resourceName: "certification")
        cell.textLabel?.text = certification.name
        cell.detailTextLabel?.text = certification.schoolName

        return cell
    }
    
    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = tableView.indexPathForSelectedRow {
            guard let destinationVC = segue.destination as? CertificationViewController else {return}
            let selectedRow = indexPath.row
            destinationVC.certification = certifications[selectedRow]
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
        if numberOfCertifications > 0 {
            return "Congratulations! You have \(numberOfCertifications) certificates."
        }
        else {
            return "No certifications yet. Let's get some!"
            }
        }
        
        return nil
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
