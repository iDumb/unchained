//
//  DetailCertificationViewController.swift
//  Unchained
//
//  Created by Marco Spina on 20/06/2020.
//  Copyright © 2020 Unchained. All rights reserved.
//

import UIKit

class DetailCertificationViewController: UIViewController {
    
    var certification = Certification()

    @IBOutlet weak var certificationNameLabel: UILabel!
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var studentNameLabel: UILabel!
    @IBOutlet weak var schoolBlockchainAddressLabel: UILabel!
    @IBOutlet weak var studentBlockchainAddressLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        certificationNameLabel.text = certification.name
        schoolNameLabel.text = certification.schoolName
        studentNameLabel.text = certification.studentName
        schoolBlockchainAddressLabel.text = certification.schoolAddress?.hex(eip55: true)
        studentBlockchainAddressLabel.text = certification.studentAddress?.hex(eip55: true)
    }
    
}
