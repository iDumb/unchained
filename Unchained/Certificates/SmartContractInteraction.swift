//
//  SmartContractInteraction.swift
//  Unchained
//
//  Created by Marco Spina on 04/06/2020.
//  Copyright © 2020 Unchained. All rights reserved.
//

import Foundation
import Web3



class SmartContractInteraction {
    
    var certification = Certification()
    
    func  getDataFromSmartContract(password: String) {
        
        let web3 = Web3(rpcURL: "https://ropsten.infura.io/v3/77fd8c86fd8942b69eaa3a76db7c49fb")

               let contractAddress = try! EthereumAddress(hex: "0xA006B7B7C276991566F20B5E9e892d32359Bddb1", eip55: true)
               let contractString =
               """
               [{"constant":true,"inputs":[],"name":"numberOfCertificates","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"string","name":"_password","type":"string"},{"internalType":"string","name":"_name","type":"string"},{"internalType":"string","name":"_schoolName","type":"string"},{"internalType":"string","name":"_studentName","type":"string"},{"internalType":"address","name":"_studentAddress","type":"address"},{"internalType":"uint256","name":"_grade","type":"uint256"},{"internalType":"string","name":"_imageLink","type":"string"}],"name":"createCertification","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"internalType":"string","name":"_password","type":"string"}],"name":"getCertificateName","outputs":[{"internalType":"string","name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"string","name":"_password","type":"string"}],"name":"getCertificateSchoolName","outputs":[{"internalType":"string","name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"string","name":"_password","type":"string"}],"name":"getCertificateSchoolAddress","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"string","name":"_password","type":"string"}],"name":"getCertificateStudentAddress","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"string","name":"_password","type":"string"}],"name":"getCertificateStudentName","outputs":[{"internalType":"string","name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"string","name":"_password","type":"string"}],"name":"getCertificateGrade","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"string","name":"_password","type":"string"}],"name":"getCertificateImageLink","outputs":[{"internalType":"string","name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"}]
               """
               let contractJsonABI = contractString.data(using: .utf8)!
                   
               // You can optionally pass an abiKey param if the actual abi is nested and not the top level element of the json
               let contract = try! web3.eth.Contract(json: contractJsonABI, abiKey: nil, address: contractAddress)
        
        firstly {
            contract["getCertificateName"]!(password).call()
        }.done { outputs in
            self.certification.name = (outputs[""] as? String)!
//            print(outputs[""] as? String)
        }.catch { error in
            print(error)
        }
        
//        print(certification.name)
        
        firstly {
            contract["getCertificateSchoolAddress"]!(password).call()
        }.done { outputs in
            self.certification.schoolAddress = (outputs[""] as? EthereumAddress)!
        }.catch { error in
            print(error)
        }
        
        firstly {
            contract["getCertificateStudentAddress"]!(password).call()
        }.done { outputs in
            self.certification.studentAddress = (outputs[""] as? EthereumAddress)!
        }.catch { error in
            print(error)
        }
        
        firstly {
            contract["getCertificateStudentName"]!(password).call()
        }.done { outputs in
            self.certification.studentName = (outputs[""] as? String)!
        }.catch { error in
            print(error)
        }
        
        firstly {
            contract["getCertificateSchoolName"]!(password).call()
        }.done { outputs in
            self.certification.schoolName = (outputs[""] as? String)!
        }.catch { error in
            print(error)
        }
        
        firstly {
            contract["getCertificateGrade"]!(password).call()
        }.done { outputs in
            self.certification.grade = (outputs[""] as? BigUInt)!
        }.catch { error in
            print(error)
        }
        
        firstly {
            contract["getCertificateImageLink"]!(password).call()
        }.done { outputs in
            self.certification.imageLink = (outputs[""] as? String)!
        }.catch { error in
            print(error)
        }
    }
    
    func getCertificationName() -> String {
        return certification.name ?? ""
    }
    
    func getCertificationSchoolName() -> String {
        return certification.schoolName ?? ""
    }
    
    func getCertificationStudentName() -> String {
        return certification.studentName ?? ""
    }
    
    func getCertificationSchoolAddress() -> EthereumAddress {
        return certification.schoolAddress!
    }
    
    func getCertificationStudentAddress() -> EthereumAddress {
        return certification.studentAddress!
    }
    
    func getCertificationGrade() -> BigUInt {
        return certification.grade ?? 0
    }
    
    func getCertificationImageLink() -> String {
        return certification.imageLink ?? ""
    }
}
