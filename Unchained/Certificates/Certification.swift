//
//  Certification.swift
//  Unchained
//
//  Created by Marco Spina on 04/06/2020.
//  Copyright © 2020 Unchained. All rights reserved.
//

import Foundation
import Web3

public class Certification: Codable {
    
    var name: String?
    var schoolName: String?
    var studentName: String?
    var studentAddress: EthereumAddress?
    var schoolAddress: EthereumAddress?
    var grade: BigUInt?
    var imageLink: String?
    
}
