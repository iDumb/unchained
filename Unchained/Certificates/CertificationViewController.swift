//
//  CertificationViewController.swift
//  Unchained
//
//  Created by Marco Spina on 19/06/2020.
//  Copyright © 2020 Unchained. All rights reserved.
//

import UIKit
import WebKit

class CertificationViewController: UIViewController, WKUIDelegate {
    
    var webView = WKWebView()
    
    var certification = Certification()

    override func viewDidLoad() {
        super.viewDidLoad()

        print(certification.name)
        
        let myURL = URL(string: certification.imageLink!)
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
    }
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
    }
    
    @IBAction func showInfo(_ sender: UIBarButtonItem) {
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destinationVC = segue.destination as? DetailCertificationViewController else {return}
        destinationVC.certification = certification
        
    }
    
}
