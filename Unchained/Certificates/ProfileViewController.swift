//
//  ProfileViewController.swift
//  Unchained
//
//  Created by Marco Spina on 04/06/2020.
//  Copyright © 2020 Unchained. All rights reserved.
//

import UIKit
import Web3
import TTGTagCollectionView



class ProfileViewController: UIViewController, TTGTextTagCollectionViewDelegate {
    
    @IBOutlet weak var interestView: UIScrollView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var projectCount: UILabel!
    @IBOutlet weak var welcomeLabel: UILabel!
    
    let collectionView = TTGTextTagCollectionView()
    let defaults = UserDefaults.standard

    
    override func viewDidLoad() {
        super.viewDidLoad()
        DataManager.shared.firstVC = self
        
        // SHOW ONBOARD SCREEN IF IS THE FIST START
        if(UserDefaults.standard.string(forKey: "name") == nil) {
            // show onboard screens
            let storyboard = UIStoryboard(name: "Onboarding", bundle: nil)
            
            let vc = storyboard.instantiateViewController(withIdentifier: "onb1") as UIViewController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false, completion: nil)

        }
        
        
        // UPLOAD THE CONUNTER OF THE PROJECT  ------------   should be tested!!!!
//        let project : [Project]
//        //let project = ProjectsManager()
//        ProjectsManager.start()
//        project = ProjectsManager.projects
//        projectCount.text = String(project.count)
//        print("project:", project.count)
        
        
        // load from userdefoults image and mame:
        
        if(UserDefaults.standard.string(forKey: "name") != nil) {
        let uImage: UIImage = UIImage(data: UserDefaults.standard.object(forKey: "profile") as! Data)!
            profileImage.image = uImage
            profileImage.maskCircle(anyImage: uImage)
        }
    

        profileImage.layer.borderWidth = 1
        profileImage.layer.borderColor = UIColor.white.cgColor
        
        if(UserDefaults.standard.string(forKey: "name") != nil) {
            let name = UserDefaults.standard.string(forKey: "name")!
            let n = splitAtFirst(str: name, delimiter: " ")?.0
            welcomeLabel.text?.append(contentsOf: n! + ",")
        }
        
        
        
        collectionView.alignment = .left
        collectionView.delegate = self
        
        
        
        interestView.addSubview(collectionView)
        
        
        

        //Testing if smart contract calls work
//
//        let certification = Certification()
//        let smartContractInteractions = SmartContractInteraction()
//        let seconds = 3.0
//
//        //getting the data from the smart contract
//        smartContractInteractions.getDataFromSmartContract(password: "3456")
//        //A password is set by the school who update the certification to the blockchain
//        //password should then be sent by the school to the students who should obtain a certifications
//        //For now I uploaded a test certification using the password 3456
//
//
//        //allowing a second to pass so the data can be retrieved
//        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
//
//            //setting the data
//            certification.name = smartContractInteractions.getCertificationName()
//            certification.studentName = smartContractInteractions.getCertificationStudentName()
//            certification.schoolAddress = smartContractInteractions.getCertificationSchoolAddress()
//            certification.grade = smartContractInteractions.getCertificationGrade()
//            certification.studentAddress = smartContractInteractions.getCertificationStudentAddress()
//            certification.imageLink = smartContractInteractions.getCertificationImageLink()
//            certification.schoolName = smartContractInteractions.getCertificationSchoolName()
//
//            print("Certification Name: \(certification.name!)")
//            print("Student Name: \(certification.studentName!)")
//            print("School Name: \(certification.schoolName!)")
//            print("School Ethereum Address: \(certification.schoolAddress!)")
//            print("Student Ethereum Address: \(certification.studentAddress!)")
//            print("Grade: \(certification.grade!)")
//            print("Link to Certification Image: \(certification.imageLink!)")
//
//
//            //test end
//
//        }
        
    }
    
    
    
    // LOAD THE COLLECTION VIEW WITH THE SELECTED ELEMENT SAVED INTO USER DEFAULTS
    func loadCollectionView(){
        
        //  SET THE STYLE OF THE CELLS
        let config = TTGTextTagConfig()
        config.backgroundColor = .systemGray4
        config.textColor = .white
        config.borderColor = .systemGray3
        config.borderWidth = 1
        config.cornerRadius = 20
        config.selectedCornerRadius = 20
        config.extraSpace = CGSize(width: 30, height: 20)
        collectionView.horizontalSpacing = 25

        
        //  SET/REFRESH THE TAGS
        collectionView.removeAllTags()
        let myarray = defaults.stringArray(forKey: "SavedInterests") ?? [String]()
        let capitalizedArray = myarray.map { $0.capitalized}
        collectionView.addTags(capitalizedArray, with: config)
        DispatchQueue.main.async {
            self.collectionView.reload()
        }

    }
    override func viewWillAppear(_ animated: Bool) {
        loadCollectionView()
        
    }
    @IBAction func plusInterest(_ sender: UIButton) {
        
    }
    
    override func viewDidLayoutSubviews() {
        collectionView.frame = CGRect(x: 10, y: 10, width: view.frame.size.width, height: 160)
    }
    
    func splitAtFirst(str: String, delimiter: String) -> (a: String, b: String)? {
       guard let upperIndex = (str.range(of: delimiter)?.upperBound), let lowerIndex = (str.range(of: delimiter)?.lowerBound) else { return nil }
       let firstPart: String = .init(str.prefix(upTo: lowerIndex))
       let lastPart: String = .init(str.suffix(from: upperIndex))
       return (firstPart, lastPart)
    }

}

// THIS CLASS IS USED FOR UPDATE THE COLLECTION VIEW FROM THE MODAL VIEW
class DataManager {
        static let shared = DataManager()
        var firstVC = ProfileViewController()
}


