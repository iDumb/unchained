//
//  AddInterestViewController.swift
//  Unchained
//
//  Created by Marco Amorosi on 14/06/2020.
//  Copyright © 2020 Unchained. All rights reserved.
//

import Foundation
import UIKit
import TTGTagCollectionView

class AddInterestViewControler: UIViewController, TTGTextTagCollectionViewDelegate {
    @IBOutlet weak var interestView: UIView!
    
    let defaults = UserDefaults.standard
    

    let collectionView = TTGTextTagCollectionView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interestView.addSubview(collectionView)
        

        // SET THE STYLE OF THE TAGS COLLECTION VIEW
        
        collectionView.alignment = .left
        collectionView.delegate = self

        let config = TTGTextTagConfig()
        config.backgroundColor = .systemGray4
        config.textColor = .white
        config.borderColor = .systemGray3
        config.borderWidth = 1
        config.cornerRadius = 15
        config.selectedCornerRadius = 15
        config.minWidth = CGFloat(182)
        config.exactHeight = CGFloat(50)
        collectionView.horizontalSpacing = 20
        collectionView.verticalSpacing = 15
        
        
        // LOADS AND AUTOSELECT TAGS FROM USER DEFAULTS
        var tags : [String]
        tags = ["BUSINESS", "DESIGN", "DEVELOPMENT", "FINANCE", "HEALTH", "INNOVATION", "LIFESTYLE", "LANGUAGES", "MANAGEMENT", "MARKETING", "MUSIC", "PHOTOGRAPHY", "PRODUCTIVITY", "SOFTWARE", "TEACHING", "VIDEO"]
        collectionView.addTags(tags, with: config)
        let mytags = defaults.stringArray(forKey: "SavedInterests") ?? [String]()

        var ind: UInt
        ind = 0

        for tag in tags{
            for myt in mytags {
                if (tag == myt){
                    collectionView.setTagAt(ind, selected: true)
                    collectionView.reload()
                }
            }
            ind += 1
        }
        // END LOADING/SELECTING
        

        

        
    }

    @IBAction func back(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    // UPLOAD TAGS AND REFRESH THE COLLECTION VIEW INTO ProfileViewController
    @IBAction func done(_ sender: UIButton) {
        defaults.set(collectionView.allSelectedTags(), forKey: "SavedInterests")
        DataManager.shared.firstVC.loadCollectionView()
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    
    func textTagCollectionView(_ textTagCollectionView: TTGTextTagCollectionView!, didTapTag tagText: String!, at index: UInt, selected: Bool, tagConfig config: TTGTextTagConfig!) {
        
     //   defaults.set(, forKey: "SavedInterestsIndex")

        //print("tcazzo:", index)
    }
    
    
    override func viewDidLayoutSubviews() {
        collectionView.frame = CGRect(x: 10, y: 10, width: view.frame.size.width, height: view.frame.size.height)
    }

    
}

