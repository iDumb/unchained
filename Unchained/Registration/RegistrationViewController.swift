//
//  EmptyView.swift
//  Unchained
//
//  Created by Marco Amorosi on 16/06/2020.
//  Copyright © 2020 Unchained. All rights reserved.
//

import Foundation
import UIKit

class RegistrationViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var jobField: UITextField!
    @IBOutlet weak var birthdayFiel: UITextField!
    @IBOutlet weak var cityField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var photoUploadButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTextFieldLayout()
        
        self.HideKeyboard()
    }
    

    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        profileImg.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func selectProfilePhotoButton(_ sender: Any) {
        var myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
        
        self.present(myPickerController, animated: true, completion: nil)
        
        // make the profile image rounded
        profileImg.maskCircle(anyImage: profileImg.image!)
        profileImg.layer.borderWidth = 3
        profileImg.layer.borderColor = UIColor.white.cgColor
        photoUploadButton.setTitle("Change Photo", for: UIControl.State.normal)
        
    }
    
    @IBAction func SaveData(_ sender: Any) {

        let imageData = profileImg.image!.pngData()
        
        // Convert to Data
        UserDefaults.standard.set(imageData, forKey: "profile")
        UserDefaults.standard.set(nameField.text! + " ", forKey: "name")
    }
    

    
    
    
    
    
    
    
    
    func setTextFieldLayout(){
        
        // set dimesnion of icons into text fields
        let mediumCOnf = UIImage.SymbolConfiguration(scale: .large)
        
        
        let nameImage = UIImage(systemName: "person.fill", withConfiguration: mediumCOnf)!
        nameImage.withTintColor(.gray,renderingMode: .alwaysOriginal)
        
        // CONFIG NAME FIELD
        nameField.setLeftIcon(nameImage)
        nameField.layer.cornerRadius = 16
        nameField.attributedPlaceholder = NSAttributedString(string: "Name Surname",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        nameField.layer.borderWidth = 1
        nameField.layer.masksToBounds = true
        
        // CONFIG JOB FIELD
        let jobImage = UIImage(systemName: "hand.thumbsup.fill", withConfiguration: mediumCOnf)!
        jobImage.withTintColor(.gray,renderingMode: .alwaysOriginal)

        jobField.setLeftIcon(jobImage)
        jobField.layer.cornerRadius = 16
        jobField.attributedPlaceholder = NSAttributedString(string: "Professional Position",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        jobField.layer.borderWidth = 1
        jobField.layer.masksToBounds = true
        
        // CONFIG BIRTH FIELD
        let birthImage = UIImage(systemName: "calendar", withConfiguration: mediumCOnf)!
        birthImage.withTintColor(.gray,renderingMode: .alwaysOriginal)

        birthdayFiel.setLeftIcon(birthImage)
        birthdayFiel.layer.cornerRadius = 16
        birthdayFiel.attributedPlaceholder = NSAttributedString(string: "month / day / year",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        birthdayFiel.layer.borderWidth = 1
        birthdayFiel.layer.masksToBounds = true
        
        
        // CONFIG CITY FIELD
        let cityImage = UIImage(systemName: "map", withConfiguration: mediumCOnf)!
        cityImage.withTintColor(.gray,renderingMode: .alwaysOriginal)

        cityField.setLeftIcon(cityImage)
        cityField.layer.cornerRadius = 16
        cityField.attributedPlaceholder = NSAttributedString(string: "City",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        cityField.layer.borderWidth = 1
        cityField.layer.masksToBounds = true
        
        
        // CONFIG EMAIL FIELD
        let emailImage = UIImage(systemName: "at", withConfiguration: mediumCOnf)!
        emailImage.withTintColor(.gray,renderingMode: .alwaysOriginal)

        emailField.setLeftIcon(emailImage)
        emailField.layer.cornerRadius = 16
        emailField.attributedPlaceholder = NSAttributedString(string: "E-mail",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        emailField.layer.borderWidth = 1
        emailField.layer.masksToBounds = true
        
        
        
        doneButton.layer.cornerRadius = 20
    }
    
    
}


extension UITextField {

 /// set icon of 20x20 with left padding of 8px
 func setLeftIcon(_ icon: UIImage) {

    let padding = 15
    let size = 22

    let outerView = UIView(frame: CGRect(x: 0, y: 0, width: size+padding, height: size) )
    let iconView  = UIImageView(frame: CGRect(x: padding, y: 0, width: size, height: size))
    iconView.image = icon
    outerView.addSubview(iconView)

    leftView = outerView
    leftViewMode = .always
  }
}

// make an UIimage rounded
extension UIImageView {
  public func maskCircle(anyImage: UIImage) {
    self.contentMode = UIView.ContentMode.scaleAspectFill
    self.layer.cornerRadius = self.frame.height / 2
    self.layer.masksToBounds = false
    self.clipsToBounds = true

   // make square(* must to make circle),
   // resize(reduce the kilobyte) and
   // fix rotation.
   self.image = anyImage
  }
}

extension UIViewController{
    func HideKeyboard(){
        let Tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DismissKeyboard))
        view.addGestureRecognizer(Tap)
    }
    
    @objc func DismissKeyboard(){
        view.endEditing(true)
    }
    
    
}
