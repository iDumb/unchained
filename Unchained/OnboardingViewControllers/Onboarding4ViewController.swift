//
//  Onboarding1ViewController.swift
//  Unchained
//
//  Created by Marco Amorosi on 21/06/2020.
//  Copyright © 2020 Unchained. All rights reserved.
//

import UIKit

class Onboarding4ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)

        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)

        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeUp.direction = .up
        self.view.addGestureRecognizer(swipeUp)

        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
    }
    
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
       if gesture.direction == .right {
            print("Swipe Right")
            let storyboard = UIStoryboard(name: "Onboarding", bundle: nil)
            
            let vc = storyboard.instantiateViewController(withIdentifier: "onb3") as UIViewController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
       }
       else if gesture.direction == .left {
            print("Swipe Left")
            let storyboard = UIStoryboard(name: "Registration", bundle: nil)
            
            let vc = storyboard.instantiateViewController(withIdentifier: "register") as UIViewController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
       }
       else if gesture.direction == .up {
            print("Swipe Up")
       }
       else if gesture.direction == .down {
            print("Swipe Down")
       }
    }
    
    
}
