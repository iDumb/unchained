//
//  ProjectsTableViewController.swift
//  Unchained
//
//  Created by Ammad on 26/05/2020.
//  Copyright © 2020 Unchained. All rights reserved.
//

import UIKit

class ProjectsTableViewController: UITableViewController {
    let defaults = UserDefaults.standard
    var folders = [Folder]()
    var filteredProjects = [Folder]()
    var selectedProjectIndexPath: IndexPath!
    let searchController = UISearchController()
    
    var isSearchBarEmpty: Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    var isFiltering: Bool {
      return searchController.isActive && !isSearchBarEmpty
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        navigationItem.leftBarButtonItem = editButtonItem
        navigationItem.searchController = searchController
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Projects"
        
        definesPresentationContext = true
        
        if let savedFolders = defaults.object(forKey: "folders") as? Data {
            let jsonDecoder = JSONDecoder()
            
            do {
                folders = try jsonDecoder.decode([Folder].self, from: savedFolders)
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    private func save() {
        let jsonEncoder = JSONEncoder()
        if let savedData = try? jsonEncoder.encode(folders) {
            defaults.set(savedData, forKey: "folders")
        } else {
            print("Failed to save folders")
        }
    }
    
    private func loadDirectories() {
        // Get the document directory url
        let fileManager = FileManager.default
        let documentURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        do {
            // Get the directory contents urls (including subfolders urls)
            let directoryContents = try fileManager.contentsOfDirectory(at: documentURL, includingPropertiesForKeys: nil)
            
            print("Successfully found directories to load !")
            
            for url in directoryContents {
                print("Found: \(url)")
                let folder = Folder(name: url.lastPathComponent, path: url)
                folders.append(folder)
            }
        } catch {
            print("Can't load directories")
            print(error.localizedDescription)
        }
    }
    
    private func deleteDirectory(name: String) {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let documentDirectory = URL(fileURLWithPath: path)
        let directory = documentDirectory.appendingPathComponent(name).path

        do {
            let fileManager = FileManager.default
            // Check if file exists
            if fileManager.fileExists(atPath: directory) {
                // Delete file
                try fileManager.removeItem(atPath: directory)
            } else {
                print("Directory does not exist!")
            }
        }
        catch {
            print(error.localizedDescription)
        }
    }
    
    @IBAction func createNewFolderPrompt(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "New Folder", message: "Please enter a name to create a new folder.", preferredStyle: .alert)
        alertController.addTextField() { textField in
            textField.placeholder = "Name"
            textField.autocapitalizationType = .sentences
        }
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        alertController.addAction(UIAlertAction(title: "Save", style: .default) { _ in
            let folderName = alertController.textFields![0].text!.isEmpty ? "Untitled Project" : alertController.textFields![0].text!
            self.createNewDirectory(folderName)
        })
        
        present(alertController, animated: true)
    }
    
    private func createNewDirectory(_ name: String) {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let documentURL = URL(fileURLWithPath: path)
        let dataPath = documentURL.appendingPathComponent(name)
        
        if !FileManager.default.fileExists(atPath: dataPath.path) {
            do {
                try FileManager.default.createDirectory(at: dataPath, withIntermediateDirectories: true, attributes: nil)
                let newDirectory = Folder(name: name, path: dataPath)
                
//                print("The folder path is: \(dataPath)")
//                print("The folder absolute path: \(dataPath.absoluteURL)")
                folders.append(newDirectory)
                
                let indexPath = IndexPath(row: self.folders.count - 1, section: 0)
                tableView.insertRows(at: [indexPath], with: .automatic)
                save()
            } catch {
                print("Can't create a new folder!")
                print(error.localizedDescription)
            }
        }
    }
    
    private func renameFolder(_ indexPath: IndexPath) {
        let alertController = UIAlertController(title: "Rename Folder", message: "Please enter a new folder name to rename \(folders[indexPath.row].name).", preferredStyle: .alert)
        alertController.addTextField() { textField in
            textField.placeholder = "Name"
            textField.autocapitalizationType = .sentences
        }
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        alertController.addAction(UIAlertAction(title: "Save", style: .default) { _ in
            let newFolderName = alertController.textFields![0].text!.isEmpty ? "Unrenamed Project" : alertController.textFields![0].text!
            var currentFolder = self.folders[indexPath.row]

            self.renameDirectory(from: currentFolder.name, to: newFolderName)

            currentFolder.name = newFolderName

            self.folders[indexPath.row] = currentFolder
            self.tableView.reloadRows(at: [indexPath], with: .automatic)
            self.save()
        })

        present(alertController, animated: true)
    }
    
    private func renameDirectory(from: String, to: String) {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let documentDirectory = URL(fileURLWithPath: path)
        let old = documentDirectory.appendingPathComponent(from)
        let new = documentDirectory.appendingPathComponent(to)

        do {
            try FileManager.default.moveItem(at: old, to: new)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let rename = UIContextualAction(style: .normal, title: "Rename") { [weak self] (action, view, completionHandler) in
            self?.renameFolder(indexPath)
            completionHandler(true)
        }
        
        rename.backgroundColor = .systemPurple
        return UISwipeActionsConfiguration(actions: [rename])
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if folders.isEmpty {
            tableView.setEmptyView(title: "No Projects", message: "Why not start adding projects?")
        } else {
            tableView.restore()
        }
        
        if isFiltering {
            return filteredProjects.count
        }
        
        return folders.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProjectCell", for: indexPath)

        // Configure the cell...
        
        let project: Folder
        
        if isFiltering {
            project = filteredProjects[indexPath.row]
        } else {
            project = folders[indexPath.row]
        }
        
        cell.imageView?.image = UIImage(systemName: "folder")
        cell.textLabel?.text = project.name
        cell.detailTextLabel?.text = "\(project.files.count)"

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedProjectIndexPath = indexPath
    }

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let alertController = UIAlertController(title: "Delete Project", message: "Are you absolutely sure you want to delete \(self.folders[indexPath.row].name)? This action cannot be undone.", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel))
            alertController.addAction(UIAlertAction(title: "Delete", style: .destructive) { _ in
                // Delete the row from the data source
                self.deleteDirectory(name: self.folders[indexPath.row].name)
                self.folders.remove(at: indexPath.row)
                self.tableView.deleteRows(at: [indexPath], with: .fade)
                self.save()
            })
            
            present(alertController, animated: true)
            
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        let movedProject = folders[fromIndexPath.row]
        folders.remove(at: fromIndexPath.row)
        folders.insert(movedProject, at: to.row)
        save()
    }

    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        guard segue.identifier == "OpenProjectSegue", let indexPath = tableView.indexPathForSelectedRow, let projectTableViewController = segue.destination as? ProjectTableViewController else {
            return
        }
        
        let project: Folder
        
        if isFiltering {
            project = filteredProjects[indexPath.row]
        } else {
            project = folders[indexPath.row]
        }
        
        projectTableViewController.project = project
        projectTableViewController.delegate = self
    }
    
    func filterContentForSearchText(_ searchText: String) {
        filteredProjects = folders.filter { project -> Bool in
            return project.name.lowercased().contains(searchText.lowercased())
        }
        
        tableView.reloadData()
    }
}

extension ProjectsTableViewController: ProjectTableViewControllerDelegate {
    func projectTableViewControllerDelegate(_ controller: ProjectTableViewController, updateProject project: Folder) {
        folders[selectedProjectIndexPath.row] = project
        tableView.cellForRow(at: selectedProjectIndexPath)?.detailTextLabel?.text = "\(project.files.count)"
        save()
    }
}

extension ProjectsTableViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
    }
}
