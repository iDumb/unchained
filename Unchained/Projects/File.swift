//
//  ProfileFile.swift
//  Unchained
//
//  Created by Ammad on 23/05/2020.
//  Copyright © 2020 Unchained. All rights reserved.
//

import UIKit

struct File: Codable {
    let name: String
    let path: URL
}
