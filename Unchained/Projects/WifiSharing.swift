//
//  wifiSharing.swift
//  Unchained
//
//  Created by Alessandro s. on 28/05/2020.
//  Copyright © 2020 Unchained. All rights reserved.
//
//

import Foundation
import UIKit
import ZIPFoundation

class WifiSharing: UIViewController {
    var webUploader: GCDWebUploader!
    var readableURL: String!
    
    /// Using temporary path to hold projects uploaded through GCDWebUploaer.
    /// After the user closes the WifiSharing segue view then the projects in this temporary path/folder will be transfered to the official projects path
    var tmpDestinationURL: URL!
    
    @IBOutlet var instructionsLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.start()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.stop()
    }
    
    
    
    // MARK: Testing
    /*
    @IBAction func saveLocally(_ sender: UIButton) {
        do {
            let tmpDestinationItems: [String] = try ProjectsManager.fileManager.contentsOfDirectory(atPath: tmpDestinationURL.path)
            for item in tmpDestinationItems {
                ProjectsManager.add(from: tmpDestinationURL.path + "/" + item, deleteSource: true)
            }
        } catch {
            print("___ Disaster 1")
        }
        
        print("___ Done moving files to official folder")
    }
    
    @IBAction func moveToIcloud(_ sender: UIButton) {
        do {
            if !ProjectsManager.fileManager.fileExists(atPath: ProjectsManager.sharedPath + "/demoProject"){
                try ProjectsManager.fileManager.copyItem(atPath: ProjectsManager.projectsPath + "/demoProject", toPath: ProjectsManager.sharedPath + "/demoProject")
            }
            
            try ProjectsManager.moveToIcloud(projectName: "demoProject")
        } catch {
            fatalError("\(error)")
        }
        
        print("___ Done moving files to icloud folder")
    }
    
    @IBAction func removeFromIcloud(_ sender: UIButton) {
        do {
            try ProjectsManager.moveToLocal(projectName: "demoProject")
        } catch {
            fatalError(error.localizedDescription)
        }
        print("___ Done removing files from icloud")
    }
    
    @IBAction func generateUrl(_ sender: UIButton) {
        guard let link = ProjectsManager.generateUrl(projectName: "demoProject")?.url else {
            print("___ Disaster 2")
            return
        }
        print("___ Generated link: \(link)")
    }
    
    @IBAction func generateHash(_ sender: UIButton) {
        print(ProjectsManager.hash(of: "demoProject"))
    }
 */
 
    func start() {
        //Create a temporary folder
        do {
            tmpDestinationURL = try FileManager.default.url(
                for: .itemReplacementDirectory,
                in: .userDomainMask,
                appropriateFor: URL(fileURLWithPath: ProjectsManager.projectsPath),
                create: true
            )
            print("WifiSharing: Tmp folder: \(tmpDestinationURL)")
        } catch {
            print("WifiSharing: Error: \(error)")
        }
        webUploader = GCDWebUploader(uploadDirectory: tmpDestinationURL.path)
        //webUploader = GCDWebUploader(uploadDirectory: "/private/var/mobile/Library/Mobile Documents/iCloud~unchained/")
        webUploader.delegate = self
        webUploader.start(withPort: 80, bonjourName: "")
        print("Started webserver")
    }
    
    func stop(){
        if !webUploader.isRunning {
            return
        }

        do {
            let tmpDestinationItems: [String] = try ProjectsManager.fileManager.contentsOfDirectory(atPath: tmpDestinationURL.path)
            for item in tmpDestinationItems {
                ProjectsManager.add(from: tmpDestinationURL.path + "/" + item, deleteSource: true)
            }
            
            //Remove temporary folder
            try ProjectsManager.fileManager.removeItem(at: tmpDestinationURL)
        } catch {
            print("WifiSharing: Error: \(error)")
        }
        
        webUploader.stop()
        webUploader = nil
        print("WebUploader Stopped")
    }
    
   
}

extension WifiSharing: GCDWebUploaderDelegate {
    func webServerDidCompleteBonjourRegistration(_ server: GCDWebServer) {
        print("Visit \(String(describing: server.bonjourServerURL?.absoluteURL)) in your web browser");
        self.readableURL = server.bonjourServerURL?.description
        
        instructionsLabel.text = "Open \(self.readableURL!) in a browser \n\n Slide down to stop Wifi Sharing"
    }
    
    func webUploader(_ uploader: GCDWebUploader, didUploadFileAtPath path: String) {
        print("Uploaded file at path: \(path)")
        let fileType = NSString(string: path).pathExtension
        print("___ File type is: \(fileType)")
        
        //Temporary veritification, in the future it should be through the first bytes (MIME-types)
        if fileType == "zip" {
            
            do {
                try ProjectsManager.fileManager.createDirectory(at: tmpDestinationURL, withIntermediateDirectories: true, attributes: nil)
                print("WifiSharing: created directory")
//                progress = Progress()
//                let progressBarTmp = UIProgressView(frame: CGRect(x: 66, y: 246, width: 282, height: 3) )
//                progressBarTmp.progress = 0
//                progressBarTmp.observedProgress = progress
//                parent?.view.addSubview(progressBarTmp)
//                print("WifiSharing: Should add progress Bar")
                try ProjectsManager.fileManager.unzipItem(at: URL(fileURLWithPath: path), to: tmpDestinationURL)
            } catch {
                print("WifiSharing: Extraction of ZIP archive failed with error:\(error)")
            }
            
            //Done unzipping zip file so now we can remove the zip file
            do {
                print("WifiSharing: Trying to delete zip file at path: \(path)")
                try FileManager.default.removeItem(at: URL(fileURLWithPath: path))
            } catch let error as NSError {
                print("WifiSharing: Error: \(error.domain)")
            }
        }
    }
}

