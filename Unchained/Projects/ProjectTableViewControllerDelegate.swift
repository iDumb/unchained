//
//  ProjectTableViewControllerDelegate.swift
//  Unchained
//
//  Created by Ammad on 18/06/2020.
//  Copyright © 2020 Unchained. All rights reserved.
//

import Foundation

protocol ProjectTableViewControllerDelegate: class {
    func projectTableViewControllerDelegate(_ controller: ProjectTableViewController, updateProject project: Folder)
}
