//
//  Project.swift
//  Unchained
//
//  Created by Ammad on 21/05/2020.
//  Copyright © 2020 Unchained. All rights reserved.
//

import UIKit

struct Project: Codable {
    var folderName: String
    var files = [File]()
    
    var name: String?
    var description: String?
    var path: String?
    var tags: [String]?
    var size: Int? //In Bytes
    // MARK: TODO
    //    var type: [ProjectType]
    //    var category: ProjectCategory?
    
//    init(folderName: String) {
//        self.folderName = folderName
//    }
}
