//
//  Folder.swift
//  Unchained
//
//  Created by Ammad on 18/06/2020.
//  Copyright © 2020 Unchained. All rights reserved.
//

import Foundation

struct Folder: Codable {
    var name: String
    var path: URL
    var files = [File]()
}
