//
//  ProjectsManager.swift
//  Unchained
//
//  Created by Alessandro s. on 08/06/2020.
//  Copyright © 2020 Unchained. All rights reserved.
//

/// TODO: Verify if user logged in icloud

import Foundation
import ZIPFoundation
import CryptoKit

/// This is the class used to write and read projects folder in the file manager
class ProjectsManager {
    
    /// The path string where all the projects are saved, in this case it's the document's folder of the user
    ///
    /// A new folder is created (if it is not already) to save all the projects
    ///
    /// Warning: Do not use this path for other things other than projects folders, this path is only supposed to contain
    static let projectsPath: String = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, .userDomainMask, true).first! + "/Projects"
    
    /// Path which contains the projects to be shared publicly. Projects in this folder are moved to icloud when possible so most of the time it is empty.
    static let sharedPath: String = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, .userDomainMask, true).first! + "/SharedProjects"
    
    /// Folder path that holds projects before they're moved to icloud. Sometime when ther
    private static let moveToIcloudPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, .userDomainMask, true).first! + "/moveToIcloud"
    
    /// The path to icloud storage
    private static let icloudPath: URL? = ProjectsManager.fileManager.url(forUbiquityContainerIdentifier: nil)
    
    //private static let sharedIcloudPath: String? = ProjectsManager.icloudPath?.path ?? "" + "/SharedProjects"
    private static var sharedIcloudPath: URL? = nil
    
    private static var projectsIcloudPath: URL? = nil
    
    /// The FileManager instance
    static let fileManager: FileManager = FileManager()
    
    
    /// Contais all the projects names inside at the projectsPath
    static var projects: [Project] = []
    
    /// Should be called only once and in the beginning of the app. It scans for every project in the folder and creates the base directory for the projects if it doesn't exist already.
    static func start() {
        if let icloudPathTmp = ProjectsManager.icloudPath {
            sharedIcloudPath =  icloudPathTmp.appendingPathComponent("SharedProjects", isDirectory: true) //icloudPathTmp.path + "/SharedProjects"
            projectsIcloudPath = icloudPathTmp.appendingPathComponent("Projects", isDirectory: true) //icloudPathTmp.path + "/Projects"
        }
        
        do {
            if !fileManager.fileExists(atPath: projectsPath) {
                try fileManager.createDirectory(atPath: projectsPath, withIntermediateDirectories: true, attributes: nil)
                print("ProjectsManager: Created directory at: \(projectsPath)")
            }
            
            if !fileManager.fileExists(atPath: sharedPath) {
                try fileManager.createDirectory(atPath: sharedPath, withIntermediateDirectories: true, attributes: nil)
                print("ProjectsManager: Created directory at: \(sharedPath)")
            }
            
            if let sharedIcloudPath = sharedIcloudPath, let projectsIcloudPath = projectsIcloudPath {
                if !fileManager.fileExists(atPath: sharedIcloudPath.path) {
                    try fileManager.createDirectory(atPath: sharedIcloudPath.path, withIntermediateDirectories: true, attributes: nil)
                    print("ProjectsManager: Created shared directory in iCloud")
                }
                
                if !fileManager.fileExists(atPath: projectsIcloudPath.path) {
                    try fileManager.createDirectory(atPath: projectsIcloudPath.path, withIntermediateDirectories: true, attributes: nil)
                    print("ProjectsManager: Created projects directory in iCloud")
                }
            }
        } catch {
            print("ProjectsManager: Errror: \(error)")
        }
        
        scanProjectsFolder()
    }
    
    /// Searches for folders inside the projects folder. Each folder should be a project.
    static func scanProjectsFolder(){
        var projectsFound: [Project] = []
        var itemsFound: [String] = []
        
        do {
            itemsFound = try fileManager.contentsOfDirectory(atPath: projectsPath)
        } catch {
            print("ProjectsManager: An error ocurred trying to read the contents of a directory at: \(projectsPath)")
        }
        
        for item in itemsFound {
            let itemPath = "\(projectsPath)/\(item)"
            
            if isDirectoryAt(itemPath) {
                let newProject = Project(folderName: item)
                projectsFound.append(newProject)
            }
            
        }
        
        projects = projectsFound
    }
    
    /// Copies a new project from a given path (must be a folder) to the official projects path
    /// - Parameter sourcePath: The path where the original project/folder is that you want to copy
    static func add(from sourcePath: String){
        
        if !isDirectoryAt(sourcePath) {
            print("ProjectManager: Only folder are allowed")
            return
        }
        
        let directoryName = (sourcePath as NSString).lastPathComponent
        
        do{
            
            try fileManager.copyItem(atPath: sourcePath, toPath: projectsPath + "/" + directoryName)
        } catch {
            print("____ \(error)")
            print("ProjectManager: An error ocurred when trying to copy item from path: \(sourcePath)")
        }
    }
    
    /// Creates a new project copied from given path (must be a folder)
    /// - Parameters:
    ///   - sourcePath: The path where the original project/folder is that you want to copy
    ///   - deleteSource: Deletes the original project/folder that was copied from, except if there's an error then it won't be deleted.
    static func add(from sourcePath: String, deleteSource: Bool){
        if !isDirectoryAt(sourcePath) {
            print("ProjectManager: Only folder are allowed")
            return
        }
        
        let directoryName = (sourcePath as NSString).lastPathComponent
        
        if deleteSource {
            do {
                try fileManager.moveItem(atPath: sourcePath, toPath: projectsPath + "/" + directoryName)
            } catch {
                print("ProjectManager: An error ocurred when trying to move item from path: \(sourcePath)")
            }
        }else{
            add(from: sourcePath)
        }
    }
    
    static func remove(projectName: String) throws{
        do {
            let localPath = URL(fileURLWithPath: projectsPath).appendingPathComponent(projectName)
            let remotePath = projectsIcloudPath!.appendingPathComponent(projectName) //The icloud path basically
            
            if fileManager.fileExists(atPath: remotePath.path) {
                try fileManager.setUbiquitous(false, itemAt: remotePath, destinationURL: localPath)
            }
            
            try fileManager.removeItem(atPath: projectsPath + "/\(projectName)")
        } catch {
            print("ProjectManager: An error ocurred trying to remove a project/folder at path: \(projectsPath)/\(projectName)")
            throw error
        }
    }
    
    static func isDirectoryAt(_ path: String) -> Bool{
        do{
            let attributes = try fileManager.attributesOfItem(atPath: path)
            let itemType = attributes[.type] as! FileAttributeType
            
            if itemType == FileAttributeType.typeDirectory {
                return true
            }else{
                return false
            }
        }catch{
            print("ProjectManager: An error ocurred trying to get attributes of item at path: \(path)")
            return false
        }
    }
    
    /// Moves the requested project to icloud container
    /// - Parameter projectName: The name of the directory where the project is saved.
    /// - Returns: Returns `nil` on success or `Error` on error
    static func moveToIcloud(projectName: String) throws{
        do {
            let projectPath = URL(fileURLWithPath: projectsPath).appendingPathComponent(projectName)
            
            try fileManager.setUbiquitous(true, itemAt: projectPath, destinationURL: projectsIcloudPath!.appendingPathComponent(projectName) )
        } catch {
            throw error
        }
    }
    
    /// Moves the project back to local path
    /// - Parameter projectName: The name of the project folder
    /// - Throws: Throws the error thrown by fileManager
    static func moveToLocal(projectName: String) throws {
        do {
            let projectPathInIcloud = projectsIcloudPath!.appendingPathComponent(projectName)
            
            try fileManager.setUbiquitous(false, itemAt: projectPathInIcloud, destinationURL: URL(fileURLWithPath: projectsPath).appendingPathComponent(projectName) )
        } catch {
            throw error
        }
    }
    
    static func generateUrl(projectName: String) -> SharedUrl? {
        do{
            let localPath = URL(fileURLWithPath: projectsPath).appendingPathComponent(projectName)
            let remotePath = projectsIcloudPath!.appendingPathComponent(projectName)
            
            let localSharedPath = URL(fileURLWithPath: sharedPath).appendingPathComponent(projectName).appendingPathExtension("zip")
            let remoteSharedPath = sharedIcloudPath!.appendingPathComponent(projectName).appendingPathExtension("zip")
            
            //Searches for the project in every folder that it could be in. Project and shared folder locally and remotely(icloud)
            if !fileManager.fileExists(atPath: remoteSharedPath.path){
                if !fileManager.fileExists(atPath: remotePath.path){
                    if !fileManager.fileExists(atPath: localSharedPath.path){
                        if !fileManager.fileExists(atPath: localPath.path){
                            return nil
                        }else{
                            try fileManager.zipItem(at: localPath, to: localSharedPath)
                            try fileManager.setUbiquitous(true, itemAt: localSharedPath, destinationURL: remoteSharedPath)
                        }
                    }else{
                         try fileManager.setUbiquitous(true, itemAt: localSharedPath, destinationURL: remoteSharedPath)
                    }
                }else{
                    try fileManager.zipItem(at: remotePath, to: localSharedPath)
                    try fileManager.setUbiquitous(true, itemAt: localSharedPath, destinationURL: remoteSharedPath)
                }
            }
            
            var date: NSDate? = NSDate()
            let datePointer = UnsafeMutablePointer<NSDate?>.allocate(capacity: 1)
            datePointer.initialize(from: &date, count: 1)
            let expDate = AutoreleasingUnsafeMutablePointer<NSDate?>(datePointer)

            let sharedUrl = try fileManager.url(forPublishingUbiquitousItemAt: remoteSharedPath, expiration: expDate)
            
            return SharedUrl(url: sharedUrl, expDate: expDate.pointee!)
        } catch {
            print("ProjectsManager: Error: \(error)")
            return nil
        }
    }
    
    /// Return the hash string of the project .zip. It's easier to calculate the hash of the zip file than the whole folder.
    /// - Parameter of: The name of project which is the name of the folder where it is stored
    static func hash(of projectName: String) -> String?{
        let projectPath = getProjectUrl(projectName: projectName)!
        
        do {
            
            
            
            let tmpDestinationURL = try FileManager.default.url(
                    for: .itemReplacementDirectory,
                    in: .userDomainMask,
                    appropriateFor: URL(fileURLWithPath: ProjectsManager.projectsPath),
                    create: true)
            
            try fileManager.zipItem(at: projectPath, to: tmpDestinationURL.appendingPathComponent(projectName + ".zip"))
            
            
            let projectData = try Data(contentsOf: URL(fileURLWithPath: tmpDestinationURL.path + "/\(projectName).zip"))
            let hashed = SHA256.hash(data: projectData)
            try fileManager.removeItem(at: tmpDestinationURL)
            
            return hashed.compactMap { String(format: "%02x", $0) }.joined()
        } catch {
            print(error.localizedDescription)
        }
        
            return nil
    }
    
    /// Returns the URL of the project you want. Projects can be stored locally or in icloud and this method gives you the correct path everytime
    /// - Returns: The URL  for the project
    private static func getProjectUrl(projectName: String) -> URL?{
        let localPath = URL(fileURLWithPath: projectsPath).appendingPathComponent(projectName)
        let remotePath = projectsIcloudPath!.appendingPathComponent(projectName)
        
        if fileManager.fileExists(atPath: remotePath.path){
            return remotePath
        }else if fileManager.fileExists(atPath: localPath.path){
            return localPath
        }else{
            return nil
        }
    }
}

struct SharedUrl {
    let url: URL!
    let expDate: NSDate!
}
