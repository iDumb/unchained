//
//  ProjectTableViewController.swift
//  Unchained
//
//  Created by Ammad on 26/05/2020.
//  Copyright © 2020 Unchained. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

class ProjectTableViewController: UITableViewController {
    weak var delegate: ProjectTableViewControllerDelegate?
    var project: Folder!
    let searchController = UISearchController()
    var ellipsisCircleButton: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
//        self.navigationItem.rightBarButtonItem = self.editButtonItem
        // navigationItem.rightBarButtonItems = [UIBarButtonItem(barButtonSystemItem: .add, target: nil, action: nil), editButtonItem]
        //navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: nil, action: nil)

        navigationItem.searchController = searchController
        title = project.name
        ellipsisCircleButton = navigationItem.rightBarButtonItem
        
        loadDirectories()
    }
    
    private func loadDirectories() {
        // Get the document directory url
        let documentURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let projectPath = documentURL.appendingPathComponent(project.name).absoluteURL
        
        do {
            let files = try FileManager.default.contentsOfDirectory(at: projectPath, includingPropertiesForKeys: nil, options: [])
            print("Success! We found:")
            for pink in files {
                print(pink)
            }
        } catch {
            print("EPIC FAIL")
            print(error)
        }
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        
        if !isEditing {
            navigationItem.rightBarButtonItem = ellipsisCircleButton
        }
    }
    
    @IBAction func addNewFile(_ sender: UIBarButtonItem) {
        importDocument()
    }
    
    func importDocument() {
        let document = UIDocumentPickerViewController(documentTypes: ["public.item"], in: .import)
        document.delegate = self
        
        present(document, animated: true)
    }
    
    private func deleteFile(_ file: File) {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let documentDirectory = URL(fileURLWithPath: path)
        
        do {
            try FileManager.default.removeItem(at: file.path)
        } catch {
            print("Could not delete file because: \(error)")
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if project.files.isEmpty {
            tableView.setEmptyView(title: "No Files", message: "Why not start adding files to this project?")
        } else {
            tableView.restore()
        }

        return project.files.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FileCell", for: indexPath)

        // Configure the cell...
        cell.imageView?.image = UIImage(systemName: "doc")
        cell.textLabel?.text = project.files[indexPath.row].name

        return cell
    }
 
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
 
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            deleteFile(project.files[indexPath.row])
            project.files.remove(at: indexPath.row)
            delegate?.projectTableViewControllerDelegate(self, updateProject: project)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
 
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        let movedFile = project.files[fromIndexPath.row]
        project.files.remove(at: fromIndexPath.row)
        project.files.insert(movedFile, at: to.row)
        delegate?.projectTableViewControllerDelegate(self, updateProject: project)
    }
 
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// Allow multiples files?

extension ProjectTableViewController: UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        let url = urls.first
        
        let fileName = url!.deletingPathExtension().lastPathComponent
        
        let file = File(name: (url?.deletingPathExtension().lastPathComponent)!, path: url!)
        project.files.append(file)
        
        let indexPath = IndexPath(row: project.files.count - 1, section: 0)
        tableView.insertRows(at: [indexPath], with: .automatic)
        
        delegate?.projectTableViewControllerDelegate(self, updateProject: project)
        
        print(project.path.appendingPathComponent(url!.lastPathComponent))
        
        let filePath = project.path.appendingPathComponent(url!.lastPathComponent)
        
        let pink = FileManager.default
        pink.createFile(atPath: filePath.path, contents: url?.dataRepresentation, attributes: nil)
    }
}
